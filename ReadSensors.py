import time
import graphyte
from  DataLogger2 import Temperature
from SHT75 import SHT_Data
import sht_sensor
from Sorensen import Sorensen
from PowerTools import *
import json
import sys
from piplates import DAQCplate as DAQ


with open('configuration.json') as json_data:
    configs= json.load(json_data)

#configurations
ovp=configs["Sorensen"]["OVP"]
clk_left=configs["SHT"]["left_clock"]
clk_right=configs["SHT"]["right_clock"]
data_left=configs["SHT"]["left_data"]
data_right=configs["SHT"]["right_data"]
voltage=configs["Sorensen"]["voltage"]
current=configs["Sorensen"]["maximum_current"]
print("---------SHT settings----")
print("SHT clock right",clk_right)
print("SHT data right",data_right)
print("SHT clock left",clk_left)
print("SHT data left",data_left)
print ""
PowerSupply=Sorensen(location=configs["Sorensen"]["location"])
PowerSupply.TurnOff()
PowerSupply.SetVProtection(ovp,1)  #DO NOT FORGET
PowerSupply.SetVProtection(ovp,2)

PowerSupply.SetVoltage(voltage,1)
PowerSupply.SetVoltage(voltage,2)
PowerSupply.SetCurrent(current,1)
PowerSupply.SetCurrent(current,2)

print("Read Voltage setting channel 1: "+str(PowerSupply.ReadSetVoltage(1)))
print("Read Voltage setting channel 2: "+str(PowerSupply.ReadSetVoltage(2)))
print("Read Current setting channel 1: "+str(PowerSupply.ReadSetCurrent(1)))
print("Read Current setting channel 2: "+str(PowerSupply.ReadSetCurrent(2)))
print("Read Voltage Protection setting Channel 1: "+str(PowerSupply.GetVProtection(1)))
print("Voltage Protection setting Channel 2: "+str(PowerSupply.GetVProtection(2)))
confirm=raw_input("Are these settings ok ? [yes/no]: ")

if(not ('yes' in confirm)):
    print("Exiting..")
    sys.exit(0)
else:
    pass

power_on=raw_input("Turn on power supply? [yes/no]")
if('yes' in power_on):
    PowerSupply.TurnOn()

Flow_voltage=DAQ.getADC(0,7)
offset=0.008
print("flow voltage ="+str(Flow_voltage+offset))
Flow_rate=FlowRate(Flow_voltage)
graphyte.init(configs['Grafana'])

while True:

    I1=PowerSupply.ReadCurrent(1)
    I2=PowerSupply.ReadCurrent(2)
    V1=PowerSupply.ReadVoltage(1)
    V2=PowerSupply.ReadVoltage(2)
    if(not (V1<ovp and V2<ovp)):  #can never be to sure
        PowerSupply.TurnOff()
        print("software ovp triggered")
        break
   
    try:
        try:
            t1,t2,t3,t4,mode=Temperature() #four channals of data logger, mode should be C or F if everything is right, X if there is a problem
            if(mode=='X'):
                raise ValueError
                #print(mode)
            print "DataLogger Temperatures"
            print t1,t2
            deltaT=t2-t1
        except ValueError:
            print "Data Logger problem, maybe its not plugged in & turned on?"

        try:
            #sht sensors
            print("V1, V2, I1,I2=",V1,V2,I1,I2)
            Electrical_power=ElectricalPower(V1,V2,I1,I2)
            ambient_temp_left,humidity_left=SHT_Data(clk_left,data_left)
            ambient_temp_right,humidity_right=SHT_Data(clk_right,data_right)
            Flow_voltage=DAQ.getADC(0,7)
            Flow_rate=FlowRate(Flow_voltage+offset)
            
            Total_power=TotalPower(Flow_rate,deltaT,(t1+t2)/2)
            Convective_power=Total_power-Electrical_power
            print("Flow Voltage: "+str(Flow_voltage+offset))
            print "SHT75 data"
            print ambient_temp_right,humidity_right
            print ambient_temp_left,humidity_left
            print("power: Total, electrical, convective")
            print(Total_power,Electrical_power,Convective_power)

            if( t1>-51 and t1<51):  
                graphyte.send("Stave.Temp.inlet",t1)
            if(t2>-51 and t2<51):
                graphyte.send("Stave.Temp.outlet",t2)



            graphyte.send("Stave.Power.Total_power",Total_power)
            graphyte.send("Stave.Power.Electrical_power",Electrical_power)
            graphyte.send("Stave.Power.Convective_power",Convective_power)

            graphyte.send("Stave.SHT75Temp.left",ambient_temp_left)
            graphyte.send("Stave.SHT75Humid.left",humidity_left)

            graphyte.send("Stave.SHT75Temp.right",ambient_temp_right)
            graphyte.send("Stave.SHT75Humid.right",humidity_right)

            graphyte.send("Stave.SHT75DeltaT",deltaT)

            print "sent"
            print "------------------------------"
            time.sleep(configs['Wait_time'])
        except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure,NameError):
            print("encountered an error check equipment if error persists")

    except KeyboardInterrupt:
        print ""
        print("exiting gracefully")
        break

PowerSupply.TurnOff()
PowerSupply.ToLocal()
PowerSupply.Close()
