import serial
import time
import binascii
import graphyte

def main():
    graphyte.init('10.2.253.74')
    while True:
        try:
            t1,t2,t3,t4,mode=temperature()
            print(t1,t2)
            graphyte.send("Stave.Temp.inlet",t1)
            graphyte.send("Stave.Temp.outlet",t2)
        except ValueError:
            print("Are you sure that its on?")
        time.sleep(5)

def temperature():
##    
##    command = "\x4B" returns 4 bytes but unknown meaning
##    command = "\x41" returns 45 bytes (8x5 + 5 = 45) as follows:
##    main()

##    "\x02\x80\xYY\xYY\xYY\xYY\xYY\xAA"  \x80 means "Celsi" (if \x00 then "Faren") YYs unknown
##    "\xAA\xBB\xBB\xCC\xCC\xDD\xDD\x00"  Temprerature T1 = AAAA, T2=BBBB, T3= CCCC, T4 = DDDD
##    "\x00\x00\x00\x00\x00\x00\x00\x00"  unknown (possible data containers but found empty)
##    "\x00\x00\x00\x00\x00\x00\x00\x00"  unknown
##    "\x00\x00\x00\x00\x00\x00\x00\x00"  unknown
##    "\x00\x00\x00\x0E\x03"              The byte r[43] \x0E changes depending on what thermocouple(s) are connected.
##                                        If T1 thermocouple connected alone, then r[43]  = \x0E = 14
##                                        If T2 thermocouple connected alone, then r[43]  = \x0D = 13
##                                        If T1 + T2 thermocouples connected, then r[43]  = \x0C = 12
##                                        If T3 thermocouple connected alone, then r[43]  = \x0B = 11
##                                        If T4 thermocouple connected alone, then r[43]  = \x07 = 7
##                                        Note: Print r[43] if you want to find other connect-combinations
##    
##    
    serCENTER = None
    try:
        ##########   CHANGE HERE  "COMx" port to match your computer   ################################
        
        port = "/dev/ttyUSB0"
        serCENTER = serial.Serial(port, baudrate=9600, bytesize=8, parity='N', stopbits=1, timeout=1)
        
        command = "\x41"                  #this comand makes the meter answer back with 45 bytes
        serCENTER.write(command.encode())
        r = serCENTER.read(45)
        serCENTER.close()
        
        if len(r) != 45:
            #Bad RX data
            return 0.,0.,"RX failed"
        #check that T1 and T2 are both plugged in 
        #print(binascii.hexlify(r[43]))       
        #if int(binascii.hexlify(r[43].encode('utf-8')),16) != 12:
        #    #T1 + T2 not plugged in
        #    return 0.,0.,"plug T1&T2!"
       

        mode = "X"
        checkmode = int(r[1])
        if checkmode == 128:
            mode = "C"          #Celsius
        elif checkmode == 0:
            mode = "F"          #Farenheit2
        #T1 = int(r[8]- r[7])
        T1=int(r[7]*256+r[8])
        if(T1/10>127):
            T1=int(r[8]-(256%r[7])*256)
            
        T2=int(r[9]*256+r[10])
        if(T2/10>127):
            T2=int(r[10]-(256%r[9])*256)
            
                       
        T3 = int(r[12]- r[11])     
        T4 = int(r[13]- r[13])     
        #print(T1,T2,T3,T4)
        return (T1/10.,T2/10.,T3/10.,T4/10.,mode)

    except serial.SerialException as e:
        print("Serial port error" + str(e))
        return (0,0,0,0,'error')


#main()
