#!/usr/bin/env python
# This file is /usr/share/cacti/scripts/humidity.py
# Read the SHT device to get humidity and temperature

#how to connect form left to right
#pin one- clock pin #13 (GPIO27)
#pin two- data pin, #15 (GPIO22)
#pin three -GND pin #6
#pin four - 3.3 V PWE, #1

import string
import sys
import time
import graphyte
from piplates import DAQCplate as DAQ
from ShtSensor import Sht

def SHT_Data(dataPin,clkPin):
    #dataPin = 12
    #dataPin=18
    #clkPin = 11
    #clkPin=17
    sht = Sht(clkPin, dataPin)
    t= sht.read_t()
    h= sht.read_rh(t)
    return (t,h)

#make LEDS on Pi flash if humidity is high, maybe will think
#of something better later.
def stupid_alarm(humidity,cut_off):
    if(h>cut_off):
        DAQ.setDOUTall(0,127)
    else:
        DAQ.setDOUTall(0,0)

def main():
    graphyte.init("10.2.253.74")
    
    while True:
        print( 'Temperature',t)
        print ('Relative Humidity',h)
        graphyte.send("Stave.SHTTemp",t)
        graphyte.send("Stave.SHTHumid",h)
        time.sleep(1)

def Test():
    t,h=SHT_Data(18,17)
    t_right,h_right=SHT_Data(22,23)
    print("SHT left")
    print(t,h)
    print("SHT right")
    print(t_right,h_right)

Test()
