import sys,os

def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__

def FlowRate(voltage):
    voltage=float(voltage)
    return voltage*16.956-6.5564

def ElectricalPower(v1,v2,i1,i2):
    v1=float(v1)
    v2=float(v2)
    i1=float(i1)
    i2=float(i2)
    return v1*i1+v2*i2

def Density(Temperature):
    den=-2.2690*Temperature+1538.3 #kg/m^3
    return den/1000.0 #g/cm^3

def SpecificHeat(Temperature):
    Cv=2.0*Temperature+1133.0 #J/kg*C
    return Cv/1000.0 #J/g*C

def TotalPower(FlowRate,deltaT,temp):
    density=Density(temp)
    Heat_capacity=SpecificHeat(temp)
    return density*FlowRate*deltaT*Heat_capacity

if __name__=="__main__":
    room_density=Density(20)
    low_temp_density=Density(-30)
    room_hc=SpecificHeat(20)
    low_temp_hc=SpecificHeat(-30)

    room_value=room_density*room_hc
    print("room value =",room_value)
    
    low_temp_value=low_temp_density*low_temp_hc
    print("low temp value",low_temp_value)
    
    difference=(room_value-low_temp_value)/room_value
    print("difference=",difference)
