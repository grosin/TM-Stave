import time
import graphyte
from  DataLogger2 import Temperature
from SHT75 import SHT_Data
import sht_sensor
from Sorensen import Sorensen
from PowerTools import *
import json
import sys

Flow_voltage=raw_input("enter flow voltage: ")

Flow_rate=FlowRate(Flow_voltage)

with open('configuration.json') as json_data:
    configs= json.load(json_data)

ovp=configs["Sorensen"]["OVP"]
clk_left=configs["SHT"]["left_clock"]
clk_right=configs["SHT"]["left_clock"]
data_left=configs["SHT"]["left_data"]
data_right=configs["SHT"]["right_data"]

graphyte.init(configs['Grafana'])
PowerSupply=Sorensen(location='/dev/ttyACM0')
PowerSupply.SetVProtection(ovp,1)  #DO NOT FORGET
PowerSupply.SetVProtection(ovp,2)




while True:

    I1=PowerSupply.ReadCurret(1)
    I2=PowerSupply.ReadCurrent(2)
    V1=PowerSupply.ReadVoltage(1)
    V2=PowerSupply.ReadVoltage(2)
    if(not (v1<ovp and v2<ovp)):  #can never be to sure
       PowerSupply.TurnOff()
       print("software ovp triggered")
       break
   
    try:
        try:
            t1,t2,t3,t4,mode=Temperature() #four channals of data logger, mode should be C or F if everything is right, X if there is a problem
            if(mode=='X'):
                raise ValueError

            print "DataLogger Temperatures"
            print t1,t2
            deltaT=t2-t1
        except ValueError:
            print "Data Logger problem, maybe its not plugged in & turned on?"

        try:
            #sht sensors
            print("V1, V2, I1,I2=",V1,V2,I1,I2)
            Electrical_power=ElectricalPower(V1,V2,I1,I2)
            ambient_temp_left,humidity_left=SHT_Data(18,17)
            ambient_temp_right,humidity_right=SHT_Data(22,23)

            Total_power=TotalPower(Flow_rate,deltaT,(t1+t2)/2)
            Convective_power=Total_power-Electrical_power

            print "SHT75 data"
            print ambient_temp_right,humidity_right
            print ambient_temp_left,humidity_left
            print("power: Total, electrical, convective")
            print(Total_power,Electrical_power,Convective_power)

            if( t1>-51 and t1<51):       #I'm only sure it works for -51 < t < 51, no clue about outside of that range, let me know if you try!
                graphyte.send("Stave.Temp.inlet",t1)
            if(t2>-51 and t2<51):
                graphyte.send("Stave.Temp.outlet",t2)



            graphyte.send("Stave.Power.Total_power",Total_power)
            graphyte.send("Stave.Power.Electrical_power",Electrical_power)
            graphyte.send("Stave.Power.Convective_power",Convective_power)

            graphyte.send("Stave.SHT75Temp.left",ambient_temp_left)
            graphyte.send("Stave.SHT75Humid.left",humidity_left)

            graphyte.send("Stave.SHT75Temp.right",ambient_temp_right)
            graphyte.send("Stave.SHT75Humid.right",humidity_right)

            graphyte.send("Stave.SHT75DeltaT",deltaT)

            print "sent"
            print "------------------------------"
            time.sleep(configs['Wait_time'])
        except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure):
            print("encountered an error with SHT, probably nothing but check equipment if error persists")

    except KeyboardInterrupt:
        print("exiting gracefully")
        break

PowerSupply.TurnOff()
PowerSupply.ToLocal()
PowerSupply.Close()
